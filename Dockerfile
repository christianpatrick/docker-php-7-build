FROM centos:centos7
MAINTAINER christianpatrick

RUN yum -y update
RUN yum -y install epel-release
RUN yum -y install wget

RUN wget http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
RUN rpm -Uvh remi-release-7*.rpm
RUN yum-config-manager --enable remi-php70
RUN rpm -Uvh http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm

RUN yum -y update
RUN wget http://repo.mysql.com/mysql-community-release-el7-5.noarch.rpm
RUN rpm -ivh mysql-community-release-el7-5.noarch.rpm
RUN yum -y install httpd php php-common php-mbstring php-mcrypt	php-devel php-xml php-intl php-mysqlnd php-pdo php-opcache php-bcmath pwgen supervisor bash-completion openssh-server psmisc tar phpmyadmin; yum clean all
RUN rm /etc/httpd/conf.d/welcome.conf /etc/httpd/conf.d/phpMyAdmin.conf /etc/php.ini
ADD ./conf/ioncube_loader_lin_7.0.so /usr/lib64/php/modules
ADD ./conf/start.sh /start.sh
ADD ./conf/foreground.sh /etc/apache2/foreground.sh
ADD ./conf/supervisord.conf /etc/supervisord.conf
ADD ./conf/phpMyAdmin.conf /etc/httpd/conf.d/phpMyAdmin.conf
ADD ./conf/httpd.conf /etc/httpd/conf/httpd.conf
ADD ./conf/php.ini /etc/php.ini
RUN echo %sudo	ALL=NOPASSWD: ALL >> /etc/sudoers
RUN chown -R apache:apache /var/www/
RUN chmod 755 /start.sh
RUN chmod 755 /etc/apache2/foreground.sh
RUN mkdir /var/run/sshd

EXPOSE 80
EXPOSE 22
EXPOSE 3306

CMD ["/bin/bash", "/start.sh"]


#docker build --rm=true --no-cache=true -t phpbuild .

#docker run -d -p 80 -p 22 -p 3306 -v /Users/christianpatrick/Documents/GitHub/eval-builder:/var/www/html phpbuild
#docker run -d -p 80 -p 22 -p 3306 -v /Users/christianpatrick/Documents/GitHub/newsmanager:/var/www/html phpbuild