#!/bin/bash

__create_user() {
# Create a user to SSH into as.
SSH_USERPASS=`pwgen -c -n -1 8`
useradd -G wheel user
echo user:$SSH_USERPASS | chpasswd
echo ssh user password: $SSH_USERPASS
}

__mysql_config() {
# Hack to get MySQL up and running... I need to look into it more.
# yum -y erase mariadb mariadb-server
# rm -rf /var/lib/mysql/ /etc/my.cnf
yum -y install mariadb mariadb-server
mysql_install_db
chown -R mysql:mysql /var/lib/mysql
mysql_install_db --user mysql > /dev/null
mysqld_safe --user mysql &
sleep 5s
}

__start_mysql() {

mysql -v < /var/www/html/localhost.sql
sleep 5s
killall mysqld
sleep 10s
}

__run_supervisor() {
supervisord -n
}

# Call all functions
__create_user
__mysql_config
__start_mysql
__run_supervisor
